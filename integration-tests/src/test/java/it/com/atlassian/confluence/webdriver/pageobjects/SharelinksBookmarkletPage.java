package it.com.atlassian.confluence.webdriver.pageobjects;

import com.atlassian.confluence.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.pageobjects.elements.query.Poller.by;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.is;

public class SharelinksBookmarkletPage extends ConfluenceAbstractPage {
    @ElementBy(id = "bookmarklet-url")
    private PageElement urlField;

    @ElementBy(id = "bookmarklet-title")
    private PageElement titleField;

    @ElementBy(id = "bookmarklet-submit")
    private PageElement submit;

    @ElementBy(id = "bookmarklet-comment")
    private PageElement commentField;

    @ElementBy(cssSelector = ".bookmarklet-section .aui-message")
    private PageElement message;

    @Override
    public String getUrl() {
        return "/plugins/sharelinksbookmarklet/bookmarklet.action";
    }

    public String getMessageLink() {
        return message.find(By.cssSelector("a")).getAttribute("href");
    }

    public TimedCondition isMessageShown() {
        return message.timed().isPresent();
    }

    public TimedCondition isFormShown() {
        return urlField.timed().isPresent();
    }

    public SharelinksBookmarkletPage submit() {
        submit.click();
        return this;
    }

    public SharelinksBookmarkletPage setUrl(String url) {
        urlField.clear().type(url);
        urlField.javascript().execute("jQuery(arguments[0]).trigger(\"change\")");
        return this;
    }

    public SharelinksBookmarkletPage setTitle(String title) {
        titleField.clear().type(title);
        return this;
    }

    public String getURLValidationError() {
        WebElement errorDiv = driver.findElement(By.cssSelector("#bookmarklet-url ~ .error"));
        return errorDiv.getText();
    }

    public String getTitleValidationError() {
        WebElement errorDiv = driver.findElement(By.cssSelector("#bookmarklet-title + .error"));
        return errorDiv.getText();
    }

    public SharelinksBookmarkletPage waitForCreatePageResultLoaded() {
        waitUntilTrue("Bookmarklet result text should be visible", pageElementFinder.find(By.cssSelector("div.bookmarklet-result-text a")).timed()
                .isVisible());
        return this;
    }

    public String getCreatedPageId() {
        waitForCreatePageResultLoaded();
        WebElement createdPageLink = driver.findElement(By.cssSelector("div.bookmarklet-result-text a"));
        String linkHref = createdPageLink.getAttribute("href");
        String pageIdSearch = "pageId=";
        int pageIdIndex = linkHref.indexOf(pageIdSearch);
        return linkHref.substring(pageIdIndex + pageIdSearch.length());
    }

    public SharelinksBookmarkletPage selectSpace(String spaceName) {
        BookmarkletSpacePicker spacePicker = pageBinder.bind(BookmarkletSpacePicker.class);
        spacePicker.inputSpace(spaceName);
        spacePicker.selectSpace(spaceName);
        return this;
    }

    public String getSelectedSpace() {
        BookmarkletSpacePicker spacePicker = pageBinder.bind(BookmarkletSpacePicker.class);
        return spacePicker.getSelectedSpace();
    }

    public SharelinksBookmarkletPage pasteUrl(String pasteValue) {
        urlField.type(pasteValue);
        urlField.javascript().execute("jQuery(arguments[0]).trigger(\"paste\")");
        return this;
    }

    public String getLoadedTitleForUrl(String title) {
        waitUntilTitleIsLoadedForUrl(title);
        return titleField.getValue();
    }

    public SharelinksBookmarkletPage waitUntilTitleIsLoadedForUrl(String title) {
        waitUntilEquals("Title field value should change", title, titleField.timed().getValue());
        return this;
    }

    public SharelinksBookmarkletPage setComment(String comment) {
        waitUntil("Comment field should be enabled", commentField.timed().isEnabled(), is(true), by(30, SECONDS));
        commentField.clear().type(comment);
        return this;
    }

    public SharelinksBookmarkletPage waitForDisabledCommentInput() {
        waitUntilFalse("Comment field should not be enabled", commentField.timed().isEnabled());
        return this;
    }

    public TimedCondition isCommentFieldEnabled() {
        return commentField.timed().isEnabled();
    }

    public SharelinksBookmarkletPage addLabel(String labelName) {
        LabelPicker labelPicker = pageBinder.bind(LabelPicker.class);
        labelPicker.inputLabel(labelName);
        labelPicker.selectLabel(labelName);
        return this;
    }
}
