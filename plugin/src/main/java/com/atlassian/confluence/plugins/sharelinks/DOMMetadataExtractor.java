package com.atlassian.confluence.plugins.sharelinks;

import org.jsoup.nodes.Document;

/**
 * @since 1.7.2
 */
public interface DOMMetadataExtractor {
    void updateMetadata(LinkMetaData meta, Document head);
}
